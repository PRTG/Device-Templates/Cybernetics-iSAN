PRTG Device Template for Cybernetics iSAN devices
===========================================


This project contains all the files necessary to integrate the Cybernetics iSAN
into PRTG for auto discovery and sensor creation.

!!! Please NOTE !!!
 Some of the values presented by the Cybernetics iSAN device through SNMP are
experimental. IE they may not be accurate.

Download Instructions
=========================
 A zip file containing all the files in the project can be downloaded from the 
repository(https://gitlab.com/PRTG-Projects/Device-Templates/Cybernetics-iSAN/-/jobs/artifacts/master/download?job=PRTGDistZip) 
download link

Installation Instructions
=========================
Please refer to INSTALL.md
